<?php
 // $Id: foursquretips.module,v 1.0$
 ?>
<div id="tipscontainer"> 
<?php
foreach($reversearray as $key){
     $tipid=$orignalarray['tipid'][$key];
     $tipPhoto=$orignalarray['photo'][$tipid];
	 $tipText=$orignalarray['text'][$tipid];
	 $tipVenue=$orignalarray['venue'][$tipid];
	 $timestamp=$orignalarray['timestamp'][$tipid];
	 $tipDate=time_ago($timestamp);
	 ?>
	 <div id='tips'>
	 <div id='tipsimage'>
	 <img class='tipsimage' src='<?php echo $tipPhoto; ?>'>
	 </div><div id='tipscontent'>
	 <p class='vname' style='margin-bottom:1px'><?php echo $tipVenue;?></p><?php echo $tipText ;?><br />
	 <span class='timeago'><?php echo $tipDate ;?></span>
	 </div>
	 </div>
	 <div style='clear:both'></div><hr>
   <?php } ?>
</div>



