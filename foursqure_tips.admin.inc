<?php
// $Id: foursquretips.module,v 1.0$
?>
<?php 

function foursquretips_admin_edit() {
$form = array();
$form['foursquretips'] = array(
'#type' => 'fieldset',
'#title' => t('Foursqure Tips'),
'#description' => t('Setting')
);
$form['foursquretips']['latitude'] = array(
'#title' => t('Enter Latitude'),
'#type' => 'textfield',
'#description' => t('Latitude of location'),
'#default_value'  =>variable_get('latitude', '40.7')
);
$form['foursquretips']['longitude'] = array(
'#title' => t('Enter Longitude'),
'#type' => 'textfield',
'#description' => t('Longitude of location'),
'#default_value'  =>variable_get('longitude', '-74')
);
$form['foursquretips']['limit'] = array(
'#title' => t('Enter Limit'),
'#type' => 'textfield',
'#description' => t('Enter Limit of tips'),
'#default_value'  =>variable_get('limit', '5')
);
$form['foursquretips']['token'] = array(
'#title' => t('Foursqure oauth_token'),
'#type' => 'textfield',
'#description' => t('Enter oauth_token given by foursqure API'),
'#default_value'  =>variable_get('token', 'ONU1CTFFDMCYHLIIX1HMPZKS2BUOFTDHYXB1VCD3SKW1FDHA')
);
return system_settings_form($form);
}

?>
